﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using System;
using UnityEngine.Events;
using InControl;

public class MovementScript : MonoBehaviour
{
    Animator animator;
    private Vector2 playerVelocity;

    public int PlayerNumber;
    public bool facingRight = true;
    [HideInInspector] public bool jump = false;
    public float moveForce = 365f;
    public float maxSpeed = 5f;
    public float maxSpeedy = 20f;
    public float jumpForce = 1000f;
    public Transform groundCheck;
    public string enenmyColorGround;
    public string color;
    public Text lifeText;
    public float ImmuneTime = 0;
    public LayerMask jumpGround;
    public LayerMask damageGround;
    public UnityEvent death;
    public int maxLives = 3;
    [HideInInspector] public int lives;
    [HideInInspector] public int life = 5;
    

    [HideInInspector] public bool grounded = false;
	[HideInInspector] public InputDevice controller;
    private bool groundeColor = false;
    private Transform gun;
    private Rigidbody2D rb2d;
    private float timeToImmune = 0;
    private bool secondJump = false;
    private Transform originalSpawn;
    private bool inControl = true;
    // Use this for initialization


    void Awake()
    {
        animator = gameObject.GetComponent<Animator>();
        animator.SetInteger("Health", 5);
        gun = transform.Find("Gun");
        controller = InputManager.Devices[PlayerNumber -1];
        originalSpawn = transform;
        lives = maxLives;
        rb2d = GetComponent<Rigidbody2D>();
        playerVelocity = rb2d.velocity;
        if (lifeText != null)
        {
            lifeText.text = color + " Life: " + lives.ToString();
        }
    }

    // Update is called once per frame
    void Update()
    {
        animator.SetInteger("Health", life);

        grounded = Physics2D.Linecast(transform.position, groundCheck.position, jumpGround);
        groundeColor = Physics2D.Linecast(transform.position, groundCheck.position, damageGround);
        if (inControl)
        {
            if (grounded)
            {
                if (controller.RightBumper.WasPressed)
                {
                    jump = true;
                }
            }
            else
            {
                if (controller.RightBumper.WasPressed)
                {
                    if (secondJump)
                    {
                        rb2d.AddForce(new Vector2(0f, jumpForce));
                        secondJump = false;
                    }
                }
            }
        }
        if (groundeColor)
        {
            takeDamage(1);
        }

        //if (playerVelocity.x > 0)
        //{
        //    animator.SetBool("Moving", true);
        //}
        //if(playerVelocity.x == 0)
        //{
        //    animator.SetBool("Moving", false);
        //}
    }


    void FixedUpdate()
    {
        float h = controller.LeftStickX.Value;
        if (life > 5)
        {
            life = 5;
        }
        if (lifeText != null)
        {
            lifeText.text = color + " Life: " + lives.ToString();
        }
        if (life < 0)
        {
            animator.SetTrigger("Dead");
            StartCoroutine(lossControl());
        }
        if (lives <= 0)
        {
            death.Invoke();
        }


        // Change animation to walking
        if (!(rb2d.velocity.x ==0) && rb2d.velocity.y ==0)
        {
            animator.SetBool("Walking", true);
        }
        if(rb2d.velocity.x == 0)
        {
            animator.SetBool("Walking", false);
        }
        if (inControl)
        {
            if (h * rb2d.velocity.x < maxSpeed)
                rb2d.AddForce(Vector2.right * h * moveForce);
            if (Mathf.Abs(rb2d.velocity.x) > maxSpeed)
                rb2d.velocity = new Vector2(Mathf.Sign(rb2d.velocity.x) * maxSpeed, rb2d.velocity.y);
            if (Mathf.Abs(rb2d.velocity.y) > maxSpeedy)
                rb2d.velocity = new Vector2(rb2d.velocity.x, Mathf.Sign(rb2d.velocity.y) * maxSpeedy);
        }
        if (h > 0 && !facingRight)
        {
            Flip();
        }

        else if (h < 0 && facingRight)
        {
            Flip();
        }
        if (jump)
        {
            rb2d.AddForce(new Vector2(0f, jumpForce));
            jump = false;
            secondJump = true;
        }

    }

    private void Respawn()
    {
        lives--;
        life = 5;
    }

    void Flip()
    {
        if (inControl)
        {
            facingRight = !facingRight;
            Vector3 theScale = transform.localScale;
            theScale.x *= -1;
            transform.localScale = theScale;
        }
    }
    public void takeDamage(int howMuch)
    {
        if (Time.time > timeToImmune)
        {
            life -= howMuch;
            StartCoroutine(Flasher());
            timeToImmune = Time.time + ImmuneTime;
        }
    }
    public void paintShoot()
    {
        life--;
    }
    public void healUp(int howMuch)
    {
        if (howMuch + life >= 5)
        {
            life = 5;
        }
        else
        {
            life += howMuch;
        }
    }
    IEnumerator Flasher()
    {
        for (int i = 0; i < 5; i++)
        {
            gameObject.GetComponent<SpriteRenderer>().color = new Color(255,255,255, 0.5f);
            yield return new WaitForSeconds(.2f);
            gameObject.GetComponent<SpriteRenderer>().color = new Color(255, 255, 255, 1f);
            yield return new WaitForSeconds(.2f);
        }
    }
    IEnumerator lossControl()
    {
        inControl = false;
        Respawn();
        gun.gameObject.SetActive(false);
        yield return new WaitForSeconds(1.25f);
        GameObject[] spawnPointsList = GameObject.FindGameObjectsWithTag(color + "Ground");
        int randomNumber = UnityEngine.Random.Range(0, spawnPointsList.Length);
        if (spawnPointsList != null && spawnPointsList.Length != 0)
        {
            GameObject spawn = spawnPointsList[randomNumber];
            transform.position = spawn.transform.position + new Vector3(0f, 2.5f);
        }
        else
        {
            death.Invoke();
        }
        yield return new WaitForSeconds(1.25f);
        inControl = true;
        gun.gameObject.SetActive(true);
    }
    public void hitSlime()
    {
        rb2d.AddForce(new Vector2(0f, 1000f));
    }

}