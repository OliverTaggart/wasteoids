﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlatformScript : MonoBehaviour {

    GameObject[] paintList;

    public void Awake()
    {
        paintList = new GameObject[4];
        for (int i = 0; i < 4; i++)
        {
            paintList[i] = transform.GetChild(i).gameObject;
        }
        foreach (GameObject item in paintList)
        {
            item.SetActive(false);
        }
    }
    public void TurnNeutral()
    {
        gameObject.layer = 12;
        gameObject.tag = "ground";
        foreach (GameObject item in paintList)
        {
            item.SetActive(false);
        }
    }
    public void TurnBlue()
    {
        gameObject.layer = 13;
        gameObject.tag = "BlueGround";
        foreach (GameObject item in paintList)
        {
            item.SetActive(false);
        }
        paintList[0].SetActive(true);
    }
    public void TurnRed()
    {
        gameObject.layer = 14;
        gameObject.tag = "RedGround";
        foreach (GameObject item in paintList)
        {
            item.SetActive(false);
        }
        paintList[1].SetActive(true);
    }
    public void TurnGreen()
    {
        gameObject.layer = 15;
        gameObject.tag = "GreenGround";
        foreach (GameObject item in paintList)
        {
            item.SetActive(false);
        }
        paintList[2].SetActive(true);
    }
    public void TurnPurple()
    {
        gameObject.layer = 16;
        gameObject.tag = "PurpleGround";
        foreach (GameObject item in paintList)
        {
            item.SetActive(false);
        }
        paintList[3].SetActive(true);
    }
}
