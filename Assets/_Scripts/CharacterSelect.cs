﻿using InControl;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class CharacterSelect : MonoBehaviour
{
    public int Player;
    GameObject[] characterList;
    InputDevice controller;
    int index;
    float selectTime;
    bool play = false;
    bool ready = false;
    GameObject bigMan;
    private void Start()
    {
        bigMan = GameObject.Find("Character Select");
        try
        {
            controller = InputManager.Devices[Player - 1];
        }
        catch (System.ArgumentOutOfRangeException)
        {

        }
        characterList = new GameObject[transform.childCount];
        for (int i = 0; i < transform.childCount; i++)
        {
            characterList[i] = transform.GetChild(i).gameObject;
        }
        foreach (GameObject item in characterList)
        {
            item.SetActive(false);
        }
    }
    private void Update()
    {
        try
        {
            controller = InputManager.Devices[Player - 1];
        }
        catch (System.ArgumentOutOfRangeException)
        {
            controller = null;
        }
        if (index != 0 && characterList[0].activeSelf)
        {
            characterList[0].SetActive(false);
        }
        if (index != 1 && characterList[1].activeSelf)
        {
            characterList[1].SetActive(false);
        }
        if (index != 2 && characterList[2].activeSelf)
        {
            characterList[2].SetActive(false);
        }
        if (index != 3 && characterList[3].activeSelf)
        {
            characterList[3].SetActive(false);
        }
        if (controller != null)
        {
            if (controller.CommandWasReleased && !play)
            {
                play = true;
                if (characterList[0])
                {
                    characterList[0].SetActive(true);
                }
            }
            if (ready)
            {
                bigMan.GetComponent<PlayersReady>().readyCheck();
                if (controller.Action2.WasReleased)
                {
                    ready = false;
                    bigMan.GetComponent<PlayersReady>().removePlayer(Player - 1, index);
                    play = true;
                    characterList[index].GetComponentInChildren<FlashingTextScript>().StartBlink();
                }
            }
            else if (play)
            {
                bigMan.GetComponent<PlayersReady>().unready();
                characterList[characterList.Length - 1].SetActive(false);
                if (controller.Action2.WasReleased)
                {
                    foreach (GameObject item in characterList)
                    {
                        item.SetActive(false);
                    }
                    play = false;
                    index = 0;
                    bigMan.GetComponent<PlayersReady>().readyCheck();
                }
                if (controller.Action1 && !(bigMan.GetComponent<PlayersReady>().selectedPlayers.Contains(index)))
                {
                    ready = true;
                    bigMan.GetComponent<PlayersReady>().addPlayer(Player - 1, index);
                    characterList[index].GetComponentInChildren<FlashingTextScript>().StopBlinking();
                }
                if (controller.LeftStickRight && Time.time > selectTime)
                {
                    selectTime = Time.time + .25f;
                    characterList[index].SetActive(false);
                    index++;
                    if (index > characterList.Length - 2)
                    {
                        index = 0;
                    }
                    characterList[index].SetActive(true);
                }
                else if (controller.LeftStickLeft && Time.time > selectTime)
                {
                    selectTime = Time.time + .25f;
                    characterList[index].SetActive(false);
                    index--;
                    if (index < 0)
                    {
                        index = characterList.Length - 2;
                    }
                    characterList[index].SetActive(true);
                }
            }
            else
            {
                characterList[index].SetActive(false);
                characterList[characterList.Length - 1].SetActive(true);
                characterList[characterList.Length - 1].GetComponentInChildren<Text>().text = "Press\nStart";
                characterList[characterList.Length - 1].GetComponentInChildren<Text>().fontSize = 25;
            }
        }

        else
        {
            play = false;
            characterList[index].SetActive(false);
            characterList[characterList.Length - 1].GetComponentInChildren<Text>().text = "Connect\nController";
            characterList[characterList.Length - 1].GetComponentInChildren<Text>().fontSize = 12;
            characterList[characterList.Length - 1].SetActive(true);
        }

    }
}
