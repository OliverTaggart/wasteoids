﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BulletMover : MonoBehaviour {

    public int speed = 230;
    GameObject thePlayer;
    bool facingRight;
    public int player;
    void Awake()
    {
        if (player == 1)
        {
            thePlayer = GameObject.Find("Player");
        }
        if (player == 2)
        {
            thePlayer = GameObject.Find("Player 2");
        }
        if (player == 3)
        {
            thePlayer = GameObject.Find("Player 3");
        }
        if (player == 4)
        {
            thePlayer = GameObject.Find("Player 4");
        }
        facingRight = thePlayer.GetComponent<MovementScript>().facingRight;
    }
        // Update is called once per frame
    void Update () {
        if (facingRight)
        {
            transform.Translate(Vector3.right * Time.deltaTime * speed);

        }
        else
        {
            transform.Translate(Vector3.left * Time.deltaTime * speed);

        }
    }
    private void OnTriggerEnter2D(Collider2D collision)
    {
        if ((collision.tag == "BlueGround" || collision.tag == "RedGround" || collision.tag == "GreenGround" || collision.tag == "PurpleGround" || collision.tag == "ground"))
        {
            if (player == 1)
            {
                collision.GetComponent<PlatformScript>().TurnBlue();
            }
            if (player == 2 )
            {
                collision.GetComponent<PlatformScript>().TurnRed();
            }
            if (player == 3)
            {
                collision.GetComponent<PlatformScript>().TurnGreen();
            }
            if (player == 4)
            {
                collision.GetComponent<PlatformScript>().TurnPurple();
            }
            Destroy(gameObject);
        }
        if (collision.tag == "Wall" || collision.tag == "PlatformSides")
        {
            transform.Rotate(0, 180, 0);
            transform.Translate(Vector3.left * Time.deltaTime * speed);
        }
    }
}
