﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using InControl;

public class PlayerSpawner : MonoBehaviour {
    public GameObject[] players;
    private void Awake()
    {
        foreach (GameObject i in players)
        {
            i.SetActive(false);
        }
        List<KeyValuePair<int, int>> playerList = GameObject.Find("GameManager").GetComponent<GameManagerScript>().playerList;
        foreach (KeyValuePair<int, int> i in playerList)
        {
            players[i.Value].SetActive(true);
            players[i.Value].GetComponent<MovementScript>().controller = InputManager.Devices[i.Key];
            players[i.Value].GetComponent<MovementScript>().controllerInt = i.Key;
            GameObject.Find("GameManager").GetComponent<GameManagerScript>().stillAlive.Add(i.Value);
        }
    }
}
