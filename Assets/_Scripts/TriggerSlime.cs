﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TriggerSlime : MonoBehaviour {

    Animator animator;
    public AudioClip hop;

    private void Start()
    {
        animator = transform.parent.gameObject.GetComponent<Animator>();
    }
    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.gameObject.tag == "Player")
        {
            if (!collision.gameObject.GetComponent<MovementScript>().grounded && collision.gameObject.GetComponent <Rigidbody2D>().velocity.y < 0)
            {
                AudioSource audio = GetComponent<AudioSource>();
                audio.Stop();
                audio.clip = hop;
                audio.Play();
                Destroy(GetComponent<Collider2D>());
                collision.gameObject.GetComponent<MovementScript>().hitSlime();
                animator.SetTrigger("Die");
                Destroy(transform.parent.gameObject, .5f);
                collision.gameObject.GetComponent<MovementScript>().healUp(3);
            }
            else 
            {
                collision.gameObject.GetComponent<MovementScript>().takeDamage(3);
            }
        }
    }
}
