﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SlimeSpawner : MonoBehaviour {
    public Transform[] slimeSpawns;
    public float slimeSpawnTimer = 10;
    public GameObject slime;
    public int numberofSlimes = 3;
    float timer = 0;
    GameObject[] slimelist;
    // Update is called once per frame
    void Update () {
        if (timer <= Time.time)
        {
            slimelist = GameObject.FindGameObjectsWithTag("Enemy");
            if (!(slimelist.Length >= numberofSlimes) && (slimeSpawns != null || slimeSpawns.Length == 0))
            {
                int rand = Random.Range(0, slimeSpawns.Length);
                Instantiate(slime, slimeSpawns[rand].position, slime.transform.rotation);
                timer = Time.time + slimeSpawnTimer;
            }
        }
    }
}
