﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MusicScript : MonoBehaviour {
    public AudioClip titleSong;
    public AudioClip inTheSkySong;
    public AudioClip sewerSong;
    public AudioClip characterSelectSong;
    public AudioClip blueSong;
    public AudioClip redSong;
    public AudioClip greenSong;
    public AudioClip purpleSong;
    public static MusicScript instance = null;
    void Awake()
    {
        //Check if instance already exists
        if (instance == null)

            //if not, set instance to this
            instance = this;

        //If instance already exists and it's not this:
        else if (instance != this)

            //Then destroy this. This enforces our singleton pattern, meaning there can only ever be one instance of a GameManager.
            Destroy(gameObject);

        //Sets this to not be destroyed when reloading scene
        DontDestroyOnLoad(gameObject);
    }
    public void PlayTitle()
    {
        AudioSource audio = GetComponent<AudioSource>();
        audio.Stop();
        audio.loop = true;
        audio.clip = titleSong;
        audio.Play();
    }
    public void PlaySky()
    {
        AudioSource audio = GetComponent<AudioSource>();
        audio.Stop();
        audio.loop = true;
        audio.clip = inTheSkySong;
        audio.Play();
    }
    public void PlaySewer()
    {
        AudioSource audio = GetComponent<AudioSource>();
        audio.Stop();
        audio.loop = true;
        audio.clip = sewerSong;
        audio.Play();
    }
    public void PlaySelect()
    {
        AudioSource audio = GetComponent<AudioSource>();
        audio.Stop();
        audio.loop = true;
        audio.clip = characterSelectSong;
        audio.Play();
    }
    public void PlayBlueWin()
    {
        AudioSource audio = GetComponent<AudioSource>();
        audio.Stop();
        audio.loop = false;
        audio.clip = blueSong;
        audio.Play();
    }
    public void PlayRedWin()
    {
        AudioSource audio = GetComponent<AudioSource>();
        audio.Stop();
        audio.loop = false;
        audio.clip = redSong;
        audio.Play();
    }
    public void PlayGreenWin()
    {
        AudioSource audio = GetComponent<AudioSource>();
        audio.Stop();
        audio.loop = false;
        audio.clip = greenSong;
        audio.Play();
    }
    public void PlayPurpleWin()
    {
        AudioSource audio = GetComponent<AudioSource>();
        audio.Stop();
        audio.loop = false;
        audio.clip = purpleSong;
        audio.Play();
    }
    public void TurnDown()
    {
        AudioSource audio = GetComponent<AudioSource>();
        audio.volume = 0.5f;
    }
    public void TurnUp()
    {
        AudioSource audio = GetComponent<AudioSource>();
        audio.volume = 1f;
    }
}
