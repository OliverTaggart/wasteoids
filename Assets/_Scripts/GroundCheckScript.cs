﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GroundCheckScript : MonoBehaviour {

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.tag == "GroundCheck" && collision.GetComponentInParent<Rigidbody2D>().velocity.y < 0 && gameObject.GetComponentInParent<MovementScript>().canTankJump)
        {
            gameObject.GetComponentInParent<MovementScript>().getJumped();
            collision.GetComponentInParent<MovementScript>().hitSlime();
        }
    }
}
