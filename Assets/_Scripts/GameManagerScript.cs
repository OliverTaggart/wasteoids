﻿using InControl;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class GameManagerScript : MonoBehaviour
{
    //public Canvas reconnectScreen;
    [HideInInspector] public bool paused = false;
    [HideInInspector] public List<KeyValuePair<int, int>> playerList;
    [HideInInspector] public List<int> stillAlive;
    public MusicScript music;

    public static GameManagerScript instance = null;

    void Awake()
    {
        music = GameObject.Find("MusicPlayer").GetComponent<MusicScript>();
        //Check if instance already exists
        if (instance == null)
        {
            instance = this;
            music.PlayTitle();
        }


        //If instance already exists and it's not this:
        else if (instance != this)

            //Then destroy this. This enforces our singleton pattern, meaning there can only ever be one instance of a GameManager.
            Destroy(gameObject);

        //Sets this to not be destroyed when reloading scene
        DontDestroyOnLoad(gameObject);
        stillAlive = new List<int>();
    }
    private void Update()
    {
        if (SceneManager.GetActiveScene().name == "PlayScene")
        {

        }
        else if (SceneManager.GetActiveScene().name == "GameStart")
        {
            if (InputManager.ActiveDevice.Command.WasPressed)
            {
                MainMenu();
            }
        }
        else if (SceneManager.GetActiveScene().name == "CharacterSelect")
        {
            playerList = GameObject.Find("Character Select").GetComponent<PlayersReady>().activePlayers;
        }
    }
    public void StartGame()
    {
        Time.timeScale = 1;
        if (playerList != null)
        {
            playerList.Clear();
            stillAlive.Clear();
        }
        SceneManager.LoadScene("CharacterSelect", LoadSceneMode.Single);
        music.PlaySelect();
    }
    public void MainMenu()
    {
        Time.timeScale = 1;
        SceneManager.LoadScene("Main Menu", LoadSceneMode.Single);
    }
    public void PlayGame()
    {
        Time.timeScale = 1;
        SceneManager.LoadScene("LevelSelect", LoadSceneMode.Single);
    }
    public void whoWin(int item)
    {
        stillAlive.Remove(item);
        if (stillAlive.Count == 1)
        {
            if (stillAlive[0] == 0)
            {
                SceneManager.LoadScene("BlueScene", LoadSceneMode.Single);
                music.PlayBlueWin();
            }
            else if (stillAlive[0] == 1)
            {
                SceneManager.LoadScene("RedScene", LoadSceneMode.Single);
                music.PlayRedWin();
            }
            else if (stillAlive[0] == 2)
            {
                SceneManager.LoadScene("GreenScene", LoadSceneMode.Single);
                music.PlayGreenWin();
            }
            else if (stillAlive[0] == 3)
            {
                SceneManager.LoadScene("PurpleScene", LoadSceneMode.Single);
                music.PlayPurpleWin();
            }
        }
    }
    public void endGame()
    {
        Application.Quit();
    }
    public void RestartGame()
    {
        music.PlayTitle();
        music.TurnUp();
        paused = false;
        SceneManager.LoadScene("GameStart", LoadSceneMode.Single);
    }
    public void PauseGame()
    {
        Time.timeScale = 0;
        music.TurnDown();
        paused = true;
    }
    public void ResumeGame()
    {
        Time.timeScale = 1;
        music.TurnUp();
        paused = false;
    }
    public void StartLevel(int i)
    {
        Time.timeScale = 1;
        if (i == 0)
        {
            music.PlaySky();
            SceneManager.LoadScene("Level1", LoadSceneMode.Single);
        }
        if (i == 1)
        {
            music.PlaySewer();
            SceneManager.LoadScene("Level2", LoadSceneMode.Single);
        }
        if (i == 2)
        {
            music.PlaySewer();
            SceneManager.LoadScene("Level3", LoadSceneMode.Single);
        }
    }
    //public void reconnectController()
    //{
    //    Time.timeScale = 0;
    //    reconnectScreen.gameObject.SetActive(true);
    //    paused = true;
    //}

}
