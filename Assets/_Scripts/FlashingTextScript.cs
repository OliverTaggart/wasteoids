﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using UnityEngine.EventSystems;

public class FlashingTextScript : MonoBehaviour
{
    Text Children;
    string originalText;
    bool blink = true;

    private void OnEnable()
    {
        if (Children != null)
        {
            StartCoroutine(BlinkText());
        }
        //Call coroutine BlinkText on Start
    }
    void Start()
    {
        Children = transform.GetChild(0).gameObject.GetComponent<Text>();
        originalText = Children.text;
        if (Children != null)
        {
            StartCoroutine(BlinkText());
        }
    }

    //function to blink the text 
    IEnumerator BlinkText()
    {
        //blink it forever. You can set a terminating condition depending upon your requirement
        while (blink)
        {
            //set the Text's text to blank
            Children.color = new Color(Children.color.r,Children.color.g,Children.color.b,1);
            //display blank text for 0.5 seconds
            yield return new WaitForSeconds(.5f);
            //display “I AM FLASHING TEXT” for the next 0.5 seconds
            if (!blink)
            {
                Children.color = new Color(Children.color.r, Children.color.g, Children.color.b, 1);
            }
            else
            {
                Children.color = new Color(Children.color.r, Children.color.g, Children.color.b, 0);
            }
            yield return new WaitForSeconds(.5f);
        }
        Children.color = new Color(Children.color.r, Children.color.g, Children.color.b, 1);
    }
    public void StopBlinking()
    {
        if (Children != null)
        {
            Children.text = "READY";
            Children.fontSize = 25;
            Children.transform.Translate(0, -1.55f, 0);
            blink = false;
        }
    }
    public void StartBlink()
    {
        if (Children != null)
        {
            Children.text = originalText;
            Children.fontSize = 20;
            Children.transform.Translate(0,1.55f, 0);
            blink = true;
            StartCoroutine(BlinkText());
        }
    }


}