﻿using InControl;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class SmallGameManager : MonoBehaviour {
    public Canvas pauseScreen;
    public Canvas controlScreen;
    GameManagerScript bigGameManager;
    bool control;
    private void Update()
    {
        if (InputManager.ActiveDevice.Command.WasPressed && !bigGameManager.paused)
        {
            Pause();
        }
        if (InputManager.ActiveDevice.Command.WasPressed && SceneManager.GetActiveScene().name == "Main Menu" && control)
        {
            ControlsStop();
        }
    }
    private void Awake()
    {
        bigGameManager = GameObject.Find("GameManager").GetComponent<GameManagerScript>();
    }

    public void ResumeButton()
    {
        if (pauseScreen != null)
        {
            pauseScreen.gameObject.SetActive(false);
        }
        bigGameManager.ResumeGame();
    }
    public void Pause()
    {
        if (pauseScreen != null)
        { 
            pauseScreen.gameObject.SetActive(true);
        }
        bigGameManager.PauseGame();
    }
    public void StartButton()
    {
        bigGameManager.StartGame();
    }
    public void Controls()
    {
        controlScreen.gameObject.SetActive(true);
        control = true;
    }
    public void ControlsStop()
    {
        controlScreen.gameObject.SetActive(false);
        control = false;
    }
    public void QuitButton()
    {
        bigGameManager.endGame();
    }
    public void RestartButton()
    {
        bigGameManager.RestartGame();
    }
}
