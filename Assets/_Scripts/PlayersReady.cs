﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using InControl;
using UnityEngine.UI;

public class PlayersReady : MonoBehaviour {

    [HideInInspector] public List<KeyValuePair<int, int>> activePlayers = new List<KeyValuePair<int, int>>();
    [HideInInspector] public bool ready;
    [HideInInspector] public List<int> selectedPlayers;
    Text canvas;
    private void Start()
    {
        canvas = transform.GetChild(0).GetComponentInChildren<Text>();
        selectedPlayers = new List<int>();
    }
    private void Update()
    {
        if (ready)
        {
            canvas.text = "PRESS START TO PLAY";
        }
        else
        {
            canvas.text = "SELECT YOUR CHARACTER";
        }
        if (ready && InputManager.ActiveDevice.Command.WasPressed)
        {
            foreach (KeyValuePair<int,int> item in activePlayers)
            {
                if (InputManager.ActiveDevice == InputManager.Devices[item.Key])
                {
                    GameObject.Find("GameManager").GetComponent<GameManagerScript>().PlayGame();
                }
            }
        }
    }
    public void addPlayer(int controller, int player)
    {
        activePlayers.Add(new KeyValuePair<int, int>(controller, player));
        selectedPlayers.Add(player);
    }
    public void removePlayer(int controller, int player)
    {
        activePlayers.Remove(new KeyValuePair<int, int>(controller, player));
        selectedPlayers.Remove(player);
    }
    public void readyCheck()
    {
        if (activePlayers.Count > 1)
        {
            ready = true;
        }
        else
        {
            ready = false;
        }
    }
    public void unready()
    {
        ready = false;
    }
}
