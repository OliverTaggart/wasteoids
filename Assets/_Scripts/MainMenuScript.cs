﻿using InControl;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class MainMenuScript: MonoBehaviour
{
    public Canvas buttons;
    public Canvas controlScreen;
    GameManagerScript bigGameManager;
    bool control;
    private void Update()
    {
        if (InputManager.ActiveDevice.Action2.WasPressed && control)
        {
            ControlsStop();
        }
    }
    private void Awake()
    {
        bigGameManager = GameObject.Find("GameManager").GetComponent<GameManagerScript>();
        controlScreen.gameObject.SetActive(false);
    }
    public void StartButton()
    {
        bigGameManager.StartGame();
    }
    public void Controls()
    {
        control = true;
        controlScreen.gameObject.SetActive(true);
        buttons.gameObject.SetActive(false);
    }
    public void ControlsStop()
    {
        control = false;
        controlScreen.gameObject.SetActive(false);
        buttons.gameObject.SetActive(true);
    }
    public void QuitButton()
    {
        bigGameManager.endGame();
    }
}
