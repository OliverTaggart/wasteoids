﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using System;
using UnityEngine.Events;
using InControl;

public class MovementScript : MonoBehaviour
{
    Animator animator;

    public AnimatorOverrideController[] overControllers;

    public int PlayerNumber;
    public bool facingRight = true;
    [HideInInspector] public bool jump = false;
    public float moveForce = 365f;
    public float maxSpeed = 5f;
    public float maxSpeedy = 20f;
    public float jumpForce = 1000f;
    public Transform groundCheck;
    public Transform upperCheck;
    public string color;
    public GameObject lifeText;
    public float ImmuneTime = 0;
    public LayerMask jumpGround;
    public LayerMask damageGround;
    public int maxLives = 3;
    public AudioClip[] sounds;
    [HideInInspector] public int lives;
    [HideInInspector] public int life = 5;
    [HideInInspector] public bool canTankJump = true;
    

    [HideInInspector] public bool grounded = false;
	[HideInInspector] public InputDevice controller;
	[HideInInspector] public int controllerInt;
    private bool groundeColor = false;
    private Transform gun;
    private Rigidbody2D rb2d;
    private float timeToImmune = 0;
    private bool secondJump = false;
    private bool inControl = true;
    private bool dead = false;
    private bool deathByJump = false;
    //private ParticleSystem pEmitter;
    //private bool disconnected = false;
    // Use this for initialization


    void Awake()
    {

        animator = gameObject.GetComponent<Animator>();
        gun = transform.Find("Gun");
        lives = maxLives;
        rb2d = GetComponent<Rigidbody2D>();
        //pEmitter = gameObject.GetComponent<ParticleSystem>();
        //pEmitter.Stop(); 

    }

    // Update is called once per frame
    void Update()
    {
        if (life >= 0)
        {
            animator.runtimeAnimatorController = overControllers[life];
        }
        else
        {
            animator.runtimeAnimatorController = overControllers[0];
        }

        grounded = Physics2D.Linecast(transform.position, groundCheck.position, jumpGround);
        groundeColor = Physics2D.Linecast(transform.position, groundCheck.position, damageGround);
        if (inControl)
        {
            if (grounded)
            {
                if (controller.RightBumper.WasPressed || controller.Action1.WasPressed)
                {
                    AudioSource audio = GetComponent<AudioSource>();
                    audio.Stop();
                    audio.clip = sounds[2];
                    audio.Play();
                    jump = true;
                    animator.SetTrigger("Jump");
                }
            }
            else
            {
                if (controller.RightBumper.WasPressed || controller.Action1.WasPressed)
                {
                    if (secondJump)
                    {
                        AudioSource audio = GetComponent<AudioSource>();
                        audio.Stop();
                        audio.clip = sounds[2];
                        audio.Play();
                        rb2d.AddForce(new Vector2(0f, jumpForce));
                        animator.SetTrigger("DoubleJump");
                        secondJump = false;
                    }
                }
            }
        }
        if (groundeColor)
        {
            takeDamage(1);
        }

        //if (playerVelocity.x > 0)
        //{
        //    animator.SetBool("Moving", true);
        //}
        //if(playerVelocity.x == 0)
        //{
        //    animator.SetBool("Moving", false);
        //}
    }


    void FixedUpdate()
    {
        float h = controller.LeftStickX.Value;
        if (life > 5)
        {
            life = 5;
        }
        if (lifeText != null)
        {
            lifeText.GetComponent<LifeScript>().display(lives);
        }
        if (life < 0)
        {
            if (!dead)
            {
                AudioSource audio = GetComponent<AudioSource>();
                audio.Stop();
                audio.clip = sounds[0];
                audio.Play();
                animator.SetBool("Jumped", false);
                dead = !dead;
                animator.SetTrigger("Dead");
                StartCoroutine(lossControl());
            }
            //StartCoroutine(lossControl());
        }
        if (lives <= 0)
        {
            ded();
        }


        // Change animation to walking
        if (!(rb2d.velocity.x ==0) && rb2d.velocity.y ==0)
        {
            animator.SetBool("Walking", true);
           // pEmitter.Play();
        }
        else if (rb2d.velocity.x == 0)
        {
            animator.SetBool("Walking", false);
           // pEmitter.Stop();
        }
        if ((rb2d.velocity.y < 0 || rb2d.velocity.y > 0 ) && grounded)
        {
            animator.SetTrigger("Landed");
        }
        else if(rb2d.velocity.y < 0)
        {
            animator.SetBool("Falling", true);
        }
        else 
        {
            animator.SetBool("Falling", false);
        }
        if (inControl)
        {
            if (h * rb2d.velocity.x < maxSpeed)
                rb2d.AddForce(Vector2.right * h * moveForce);
            if (Mathf.Abs(rb2d.velocity.x) > maxSpeed)
                rb2d.velocity = new Vector2(Mathf.Sign(rb2d.velocity.x) * maxSpeed, rb2d.velocity.y);
            if (Mathf.Abs(rb2d.velocity.y) > maxSpeedy)
                rb2d.velocity = new Vector2(rb2d.velocity.x, Mathf.Sign(rb2d.velocity.y) * maxSpeedy);
        }
        if (h > 0 && !facingRight)
        {
            Flip();
        }

        else if (h < 0 && facingRight)
        {
            Flip();
        }
        if (jump)
        {
            rb2d.AddForce(new Vector2(0f, jumpForce));
            jump = false;
            secondJump = true;
        }

    }

    private void Respawn()
    {
        AudioSource audio = GetComponent<AudioSource>();
        audio.Stop();
        audio.clip = sounds[4];
        audio.Play();
        lives--;
        life = 5;
    }

    void Flip()
    {
        if (inControl)
        {
            facingRight = !facingRight;
            Vector3 theScale = transform.localScale;
            theScale.x *= -1;
            transform.localScale = theScale;
        }
    }
    public void takeDamage(int howMuch)
    {
        if (Time.time > timeToImmune)
        {
            canTankJump = false;
            life -= howMuch;
            AudioSource audio = GetComponent<AudioSource>();
            audio.Stop();
            audio.clip = sounds[3];
            audio.Play();
            StartCoroutine(Flasher());
            timeToImmune = Time.time + ImmuneTime;
        }
    }
    public void paintShoot()
    {
        AudioSource audio = GetComponent<AudioSource>();
        audio.Stop();
        audio.clip = sounds[1];
        audio.Play();
        life--;
    }
    public void healUp(int howMuch)
    {
        if (howMuch + life >= 5)
        {
            life = 5;
        }
        else
        {
            life += howMuch;
        }
    }
    IEnumerator Flasher()
    {
        for (int i = 0; i < 5; i++)
        {
            gameObject.GetComponent<SpriteRenderer>().color = new Color(255,255,255, 0.5f);
            yield return new WaitForSeconds(.2f);
            gameObject.GetComponent<SpriteRenderer>().color = new Color(255, 255, 255, 1f);
            yield return new WaitForSeconds(.2f);
        }
        canTankJump = true;
    }
    IEnumerator lossControl()
    {
        inControl = false;
        gun.gameObject.SetActive(false);
        if (deathByJump)
        {
            yield return new WaitForSeconds(1.33f);
        }
        else
        {
            yield return new WaitForSeconds(1.25f);
        }
        GameObject[] spawnPointsList = GameObject.FindGameObjectsWithTag(color + "Ground");
        int randomNumber = UnityEngine.Random.Range(0, spawnPointsList.Length);
        if (spawnPointsList != null && spawnPointsList.Length != 0)
        {
            GameObject spawn = spawnPointsList[randomNumber];
            transform.position = spawn.transform.position + new Vector3(0f, 1.75f);
            Respawn();
            yield return new WaitForSeconds(1.25f);
            inControl = true;
            gun.gameObject.SetActive(true);
            dead = !dead;
            deathByJump = false;
        }
        else
        {
            ded();
        }

    }
    void ded()
    {
        inControl = false;
        gun.gameObject.SetActive(false);
        //yield return new WaitForSeconds(1.25f):
        gameObject.SetActive(false);
        GameObject.Find("GameManager").GetComponent<GameManagerScript>().whoWin(PlayerNumber-1);
    }
    public void hitSlime()
    {
        rb2d.AddForce(new Vector2(0f, 1000f));
    }
    public void getJumped()
    {
        deathByJump = true;
        if (!dead)
        {
            AudioSource audio = GetComponent<AudioSource>();
            audio.Stop();
            audio.clip = sounds[0];
            audio.Play();
            animator.SetBool("Jumped", true);
            dead = !dead;
            animator.SetTrigger("Dead");
            StartCoroutine(lossControl());
        }
    }

}