﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using InControl;

public class Weapon : MonoBehaviour {

    // Use this for initialization
    public float fireRate;
    public LayerMask ToHit;
    public Transform bullet;
    GameObject player;
    [HideInInspector]public int bulletCount;

    protected bool facingRight = true;
    InputDevice controller;
    float timeToFire = 0;
    Transform firePoint;
    bool paused = false;
    void Start () {
        player = transform.parent.gameObject;
        controller = InputManager.Devices[player.GetComponent<MovementScript>().controllerInt];
        firePoint = transform.Find("FirePoint");
        if (firePoint == null)
        { 
            Debug.LogError("No firepoint");
        }
	}
	
	// Update is called once per frame
	void Update () {
        paused = GameObject.Find("GameManager").GetComponent<GameManagerScript>().paused;
        bulletCount = player.GetComponent<MovementScript>().life;
        if (!paused && controller != null)
        {
            if (fireRate == 0)
            {
                if (controller.RightTrigger.WasPressed)
                {
                    Shoot();
                }
            }
            else
            {
                if (controller.RightTrigger.WasPressed && Time.time > timeToFire)
                {
                    timeToFire = Time.time + 1 / fireRate;
                    Shoot();
                }
            }
            float rotZ = Mathf.Atan2(controller.RightStickY, controller.RightStickX) * Mathf.Rad2Deg;
            if (rotZ != 0)
            {
                //if (rotZ < 23 && rotZ > -23)
                //{
                //    rotZ = 0;
                //}
                //else if (rotZ > 23 && rotZ < 68)
                //{
                //    rotZ = 45;
                //}
                //else if (rotZ > 68 && rotZ < 113)
                //{
                //    rotZ = 90;
                //}
                //else if (rotZ > 113 && rotZ < 158)
                //{
                //    rotZ = 135;
                //}
                //else if (rotZ > 158 && rotZ < -158)
                //{
                //    rotZ = 180;
                //}
                //else if (rotZ > -158 && rotZ < -113)
                //{
                //    rotZ = -135;
                //}
                //else if (rotZ > -113 && rotZ < -68)
                //{
                //    rotZ = -90;
                //}
                //else if (rotZ > -68 && rotZ < -23)
                //{
                //    rotZ = -45;
                //}
                if (player.GetComponent<MovementScript>().facingRight)
                {
                    transform.rotation = Quaternion.Euler(0, 0, rotZ);
                }
                else
                {
                    transform.rotation = Quaternion.Euler(0, 0, rotZ - 180);

                }
            }
        }
        //if (controller.RightStickUp && controller.RightStickRight)
        //{
        //    transform.rotation = Quaternion.Euler(0f, 0f, 45);
        //}
        //else if (controller.RightStickUp && controller.RightStickLeft)
        //{
        //    transform.rotation = Quaternion.Euler(0f, 0f, 130);
        //}
        //else if (controller.RightStickDown && controller.RightStickLeft)
        //{
        //    transform.rotation = Quaternion.Euler(0f, 0f, 225);
        //}
        //else if (controller.RightStickDown && controller.RightStickRight)
        //{
        //    transform.rotation = Quaternion.Euler(0f, 0f, 315);
        //}
        //else if (controller.RightStickDown)
        //{
        //    transform.rotation = Quaternion.Euler(0f, 0f, 270);

        //}
        //else if (controller.RightStickUp)
        //{
        //    transform.rotation = Quaternion.Euler(0f, 0f, 90);
        //}
        //else if (controller.RightStickRight)
        //{
        //    transform.rotation = Quaternion.Euler(0f, 0f, 0);
        //}
        //else if (controller.RightStickLeft)
        //{
        //    transform.rotation = Quaternion.Euler(0f, 0f, 180);
        //}
    }

    void Shoot()
    {
        if (bulletCount == 0)
        {
            // play sound of empty shot
        }
        else
        {
            Effect();
            player.GetComponent<MovementScript>().paintShoot();

        }
    }

    private void Effect()
    {
        Instantiate(bullet, firePoint.position, firePoint.rotation);
    }
    public void Flip()
    {
        //Vector3 theScale = transform.localScale;
        //theScale.x *= 1;
        //transform.localScale = theScale;
    }
}
