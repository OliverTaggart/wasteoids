﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LifeScript : MonoBehaviour {
    GameObject[] lives;
    private void Awake()
    {
        lives = new GameObject[transform.childCount];
        for (int i = 0; i < transform.childCount; i++)
        {
            lives[i] = transform.GetChild(i).gameObject;
        }
        foreach (GameObject item in lives)
        {
            item.SetActive(false);
        }
    }
    public void display(int number)
    {
        for (int i = 0; i < number; i++)
        {
            lives[i].SetActive(true);
        }
        for (int i = number ; i < lives.Length; i++)
        {
            lives[i].SetActive(false);
        }
    }
}
