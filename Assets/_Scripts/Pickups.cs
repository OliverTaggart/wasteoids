﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Pickups : MonoBehaviour
{

    // Use this for initialization
    public float respawnTime;
    float respawnTimer = 0;
    bool active = true;
    bool hovering = false;
    private void Update()
    {
        if (respawnTimer <= Time.time && !hovering)
        {
            gameObject.GetComponent<SpriteRenderer>().color = new Color(255,255,255);
            active = true;
        }
    }
    private void OnTriggerStay2D(Collider2D collision)
    {
        if (collision.tag == "Player" && collision.GetComponentInChildren<Weapon>().bulletCount < 5 && active && !hovering)
        {
            hovering = true;
            gameObject.GetComponent<SpriteRenderer>().color = new Color(0,0,0);
            collision.GetComponent<MovementScript>().healUp(1);
        }
    }
    private void OnTriggerExit2D(Collider2D collision)
    {
        if (collision.tag == "Player")
        {
            hovering = false;
            respawnTimer = Time.time + respawnTime;
            active = false;
        }

    }
}
