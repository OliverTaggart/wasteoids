﻿using InControl;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LevelSelect : MonoBehaviour {
    GameObject[] levelList;
    InputDevice controller;
    int index;
    float selectTime;
    bool selected;
    void Start () {

        //controller = InputManager.Devices[0];
        controller = InputManager.Devices[GameObject.Find("GameManager").GetComponent<GameManagerScript>().playerList[0].Key];
        levelList = new GameObject[transform.childCount];
        for (int i = 0; i < transform.childCount; i++)
        {
            levelList[i] = transform.GetChild(i).gameObject;
        }
        foreach (GameObject item in levelList)
        {
            item.SetActive(false);
        }
    }
	
	// Update is called once per frame
	void Update ()
    {
        if (selected)
        {
            transform.GetChild(transform.childCount - 1).gameObject.SetActive(true);
            if (controller.Action2.WasReleased)
            {
                selected = false;
                levelList[index].transform.GetChild(1).gameObject.SetActive(true);
            }
            if (controller.Command.WasPressed)
            {
                GameObject.Find("GameManager").GetComponent<GameManagerScript>().StartLevel(index);
            }
        }
        else
        {
            if (controller.Action2.WasReleased)
            {
                GameObject.Find("GameManager").GetComponent<GameManagerScript>().StartGame();
            }
            if (controller.Action1.WasReleased)
            {
                selected = true;
                levelList[index].transform.GetChild(1).gameObject.SetActive(false);
            }
            transform.GetChild(transform.childCount - 1).gameObject.SetActive(false);
            if (levelList[index])
            {
                levelList[index].SetActive(true);
            }
            if (controller.LeftStickRight && Time.time > selectTime)
            {
                selectTime = Time.time + .25f;
                levelList[index].SetActive(false);
                index++;
                if (index > levelList.Length - 2)
                {
                    index = 0;
                }
                levelList[index].SetActive(true);
            }
            else if (controller.LeftStickLeft && Time.time > selectTime)
            {
                selectTime = Time.time + .25f;
                levelList[index].SetActive(false);
                index--;
                if (index < 0)
                {
                    index = levelList.Length - 2;
                }
                levelList[index].SetActive(true);
            }
        }
    }
}
